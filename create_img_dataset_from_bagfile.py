import cv2 
import sys
import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from nav_msgs.msg import Odometry
import numpy as np


bridge = CvBridge()
class DatasetCreator:
    i = 0
    # def __init__(self):
    # file = open("testfile.txt","w") 
    go = False
    mat = np.zeros((256, 256, 1), dtype = "uint8")
    skip_count = 0

    def main(self):
      # ic = image_from_ardrone
      topic = "/ardrone/front/image_raw"
      rospy.Subscriber(topic,Image,self.callback)
      print 'Ready to begin.. Run bag file.. topic:', topic
      # rospy.Subscriber("/usb_cam/image_raw",Image,callback)
      rospy.spin()

    def callback(self,data):
        try:
            # rate = rospy.Rate(100)
            # self.go = False
            cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
            (rows,cols,channels) = cv_image.shape
            copy = cv_image.copy()
            cv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
            a = cv2.goodFeaturesToTrack(cv_img, 500, 0.01, 45)
            # w = 32
            # h = 32
            if self.i == 0:
                self.mat = cv_img
            # for patch_centre in a:
            #     # print np.asarray(patch_centre[0])
            #     # print patch_centre
            #     rect_start = (int(patch_centre[0][0])-w,int(patch_centre[0][1])-h)
            #     rect_end = (int(patch_centre[0][0])+w,int(patch_centre[0][1])+h)
            #     # print rect_start,rect_end
            #     if rect_start[0]>20 and rect_start[1]>20 and rect_end[0] < cols-20 and rect_end[1]<rows-20:
            #         cv2.circle(cv_image,(patch_centre[0][0],patch_centre[0][1]), 2, 255, 3)
            #         cv2.rectangle(cv_image,rect_start, rect_end, 255, 2)
            #         crop = copy[rect_start[1]:rect_end[1],rect_start[0]:rect_end[0]]
            #         cv2.imwrite("test_patches/patches/ptch_%04d.png"%i,crop)
            #         i +=1 
            if (len(a) > 30 and cv2.countNonZero(cv2.subtract(self.mat,cv_img))>0) or self.i == 0:
                    # print self.i
                    count = self.i
                    # self.go = True
                # cv2.imwrite("test_patches/ths.png",cv_image)
                # cv2.imwrite("test_patches/tht.png",copy)
                    cv2.imwrite("/home/saif/tools_py/test_1/data/image_%04d.png"%count,copy)
                    print count
                    # rospy.Subscriber("/odom",Odometry,self.writeToFile,(copy,count),queue_size=1)
                    self.mat = cv_img
                    self.i +=1
            else:
                    self.skip_count+=1
                    print "skipping", self.skip_count
            # rate.sleep()
        except KeyboardInterrupt:
            cv2.destroyAllWindows()
            rospy.Unsubscribe()

    # def writeToFile(self,odom,(img,counter)):
    #     if self.go:
    #         print self.i
    #         self.file.write(str(str(counter) + " " + str(odom.pose.pose.position.x) +" "+ str(odom.pose.pose.position.y) + " " + str(odom.pose.pose.position.z) + " " + str(odom.pose.pose.orientation.x) + " " + str(odom.pose.pose.orientation.y) + " " + str(odom.pose.pose.orientation.z) + " " + str(odom.pose.pose.orientation.w)+"\n"))
        # print counter
        # print odom.pose.pose
        # cv2.imshow("window",img)
        # cv2.waitKey(1)
        

if __name__ == '__main__':
    print "Initialising... Please wait..."
    dc = DatasetCreator()
    rospy.init_node('image_from_ardrone', anonymous=True)
    # rospy.wait_for_service("/ardrone/bottom/set_camera_info", 5)
    # rospy.wait_for_service("/usb_cam/set_camera_info", 5)
    dc.main()
    print "total captured: ", dc.i
    print 'total skipped: ', dc.skip_count