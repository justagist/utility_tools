#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/nonfree/nonfree.hpp>
#include <stdio.h>
#include <opencv2/core/eigen.hpp>
#include <Eigen/Geometry>
// #include <Eigen>
using namespace cv;

#include <vector>
using namespace std;
// using namespace Eigen;

int main(int argc, char** argv) {
    cv::Point3f p3D;
    cv::Point2f p2D;
    // string p3D_filename = "/home/saif/msc_workspace/ismar/S01_INPUT/S01_3Ddata_dst_init.csv";
    string TEMPL_FORMAT = "/home/saif/tools_py/test_1/patches/ptch_%04d.png";
    // FILE *p3D_f = fopen(p3D_filename.c_str(), "r");
    Mat img = imread("/home/saif/tools_py/test_1/tht.png",0);
    Mat original = imread("/home/saif/tools_py/test_1/tht.png",-1);
    int findex = 0;
    while (findex<35)
    {
        cv::Mat templ;
        char buf[256];
        sprintf(buf, TEMPL_FORMAT.c_str(), findex++);
        templ = cv::imread(buf,0);
        // resize(templ,templ,Size(templ.cols/1.5,templ.rows/1.5));

        Mat result(img.rows - templ.rows + 1,img.cols - templ.cols + 1,CV_32FC1);
        matchTemplate(img,templ,result,CV_TM_CCOEFF_NORMED);
        namedWindow("tmp");
        // imshow("tmp",templ);
        // waitKey(0);

        double maxv; Point maxp;
        minMaxLoc(result,0,&maxv,0,&maxp);
        std::cout << ((2*maxp.x)+templ.cols)/2 << " " << ((2*maxp.y)+templ.rows)/2 << std::endl;
        circle(original,Point(((2*maxp.x)+templ.cols)/2,((2*maxp.y)+templ.rows)/2),10,Scalar(0,0,255),1);
        imshow("tmp",original);
        char c = waitKey(0);
        while (c!='s')
            {
                imshow("tmp",original);
                c = waitKey(0);
            }

    }
        
    // Mat A(20, 20, CV_32FC1);
    // cv::randn(A, 0.0f, 1.0f); // random data

    // // Map the OpenCV matrix with Eigen:
    // // Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>> A_Eigen(A.ptr<float>(), A.rows, A.cols);
    // // eigen2cv(const Eigen::Matrix<_Tp, _rows, _cols, _options, _maxRows, _maxCols>& src, Mat& dst)
    // Eigen::Matrix3f dst;
    // // cv2eigen(A, dst);
    // cv::Mat_<double> a = Mat_<double>(3,3);
    // // Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> b;
    // double low = -500.0;
    // double high = +500.0;
    // randu(a, Scalar(low), Scalar(high));
    // cv2eigen(a,dst);
    // std::cout << a << std::endl;
    // std::cout << dst << std::endl;
    // Eigen::Quaternionf q(dst);
    // q.normalize();
    // std::cout << q.coeffs() << std::endl;



    // Do something with it in Eigen, create e.g. a new Eigen matrix:
    // Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> B = A_Eigen.inverse();

    // create an OpenCV Mat header for the Eigen data:
    // Mat B_OpenCV(B.rows(), B.cols(), CV_32FC1, B.data());
    return 0;
}