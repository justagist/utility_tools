import numpy as np
import cv2 
import yaml
import sys
import csv
from map_memory import Map_Memory
import roslib
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from cv_cam import image_from_ardrone
from cam_odo import vis_odom
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D



# a = [1,2,3,2,1,3,4,2,2,4]
# b = [0,3,4,2,4,6,7,5,3,2]
# c = [2,2,4,5,7,5,3,3,2,1]
# d = [0,3,4,2,4,6,7,5,3,2]
# e = [1,2,3,2,1,3,4,2,2,4]
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# bx = fig.add_subplot(111, projection='3d')
# # ax.plot((a,b,c,'bo')
# bx.plot(e,d,c,'ro',a,b,c,'k')
# plt.show()
# loaded_data =p(1)

##########  ---------------  GET GOOD PATCHES AND SAVE THEM AS PNG ----------------------
bridge = CvBridge()
def main():
  # ic = image_from_ardrone
  
  # rospy.Subscriber("/ardrone/bottom/image_raw",Image,callback)
  rospy.Subscriber("/usb_cam/image_raw",Image,callback)
  rospy.spin()

def callback(data):
    try:
        rate = rospy.Rate(100)
        cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
        (rows,cols,channels) = cv_image.shape
        copy = cv_image.copy()
        cv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        a = cv2.goodFeaturesToTrack(cv_img, 50, 0.01, 50)
        w = 32
        h = 32
        i = 0
        for patch_centre in a:
            # print np.asarray(patch_centre[0])
            # print patch_centre
            rect_start = (int(patch_centre[0][0])-w,int(patch_centre[0][1])-h)
            rect_end = (int(patch_centre[0][0])+w,int(patch_centre[0][1])+h)
            # print rect_start,rect_end
            if rect_start[0]>20 and rect_start[1]>20 and rect_end[0] < cols-20 and rect_end[1]<rows-20:
                cv2.circle(cv_image,(patch_centre[0][0],patch_centre[0][1]), 2, 255, 3)
                cv2.rectangle(cv_image,rect_start, rect_end, 255, 2)
                crop = copy[rect_start[1]:rect_end[1],rect_start[0]:rect_end[0]]
                cv2.imwrite("test_patches/patches/ptch_%04d.png"%i,crop)
                i +=1 
        print len(a),i
        cv2.imwrite("test_patches/ths.png",cv_image)
        # cv2.imwrite("test_patches/tht.png",copy)
        cv2.imshow("window",cv_image)
        cv2.waitKey(1)
        rate.sleep()
    except KeyboardInterrupt:
        cv2.destroyAllWindows()

if __name__ == '__main__':
    rospy.init_node('image_from_ardrone', anonymous=True)
    # rospy.wait_for_service("/ardrone/bottom/set_camera_info", 5)
    rospy.wait_for_service("/usb_cam/set_camera_info", 5)
    main()
###########--------------------------------------------------------------------------------

# done = True
# bridge = CvBridge()
# def main(v):
#   # ic = image_from_ardrone
  
#   rospy.Subscriber("ardrone/bottom/image_raw",Image,callback,v)
#   rospy.spin()

# def callback(data,v):
#     global done
#     if done:
#         done = False
#         rate = rospy.Rate(5)
#         cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
#         (rows,cols,channels) = cv_image.shape
#         cv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
#         patch_2d,unmatched_ids = v.match_features(cv_img,"test_patches/patches/",True)
        
#         rate.sleep()
#         if not patch_2d is None:
#             if len(patch_2d) > 15:
#                 cv2.waitKey(0)
#             else:
#                 cv2.waitKey(1)
#                 # print 'matched %d features'%len(patch_2d)
#         done = True
#         cv2.waitKey(1)

# if __name__ == '__main__':
#     rospy.init_node('image_from_ardrone', anonymous=True)
#     rospy.wait_for_service("ardrone/bottom/set_camera_info", 5)
#     rate = rospy.Rate(1000)
#     v = vis_odom()

#     main(v)

# -----------------------------------------------------------------------

# import matplotlib.pyplot as plt
# import numpy as np

# A = np.random.rand(5, 5)
# plt.figure(1)
# plt.imshow(A, interpolation='nearest')
# plt.grid(True)

# plt.figure(2)
# plt.imshow(A, interpolation='bilinear')
# plt.grid(True)

# plt.figure(3)
# plt.imshow(A, interpolation='bicubic')
# plt.grid(True)

# plt.show()