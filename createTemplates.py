# import numpy as np
import cv2 
import sys
import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image


bridge = CvBridge()
def main():
  # ic = image_from_ardrone
  
    print "Ready to begin. Load bagfile..."
    rospy.Subscriber("/ardrone/front/image_raw",Image,callback)
  # rospy.Subscriber("/usb_cam/image_raw",Image,callback)
    rospy.spin()

def callback(data):
    try:
        rate = rospy.Rate(100)
        cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
        (rows,cols,channels) = cv_image.shape
        copy = cv_image.copy()
        cv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        a = cv2.goodFeaturesToTrack(cv_img, 100, 0.01, 45)
        w = 32
        h = 32
        i = 0
        for patch_centre in a:
            # print np.asarray(patch_centre[0])
            # print patch_centre
            rect_start = (int(patch_centre[0][0])-w,int(patch_centre[0][1])-h)
            rect_end = (int(patch_centre[0][0])+w,int(patch_centre[0][1])+h)
            # print rect_start,rect_end
            if rect_start[0]>20 and rect_start[1]>20 and rect_end[0] < cols-20 and rect_end[1]<rows-20:
                cv2.circle(cv_image,(patch_centre[0][0],patch_centre[0][1]), 2, 255, 3)
                cv2.rectangle(cv_image,rect_start, rect_end, 255, 2)
                crop = copy[rect_start[1]:rect_end[1],rect_start[0]:rect_end[0]]
                cv2.imwrite("test_1/patches/ptch_%04d.png"%i,crop)
                i +=1 
        print len(a),i
        cv2.imwrite("test_1/ths.png",cv_image)
        cv2.imwrite("test_1/tht.png",copy)
        cv2.imshow("window",cv_image)
        a = cv2.waitKey(0)
        if a == 's':
            sys.exit("Saving")
        rate.sleep()
    except KeyboardInterrupt:
        cv2.destroyAllWindows()

if __name__ == '__main__':
    print "Initialising... Pls Wait..."
    rospy.init_node('image_from_ardrone', anonymous=True)
    # rospy.wait_for_service("/ardrone/bottom/set_camera_info", 5)
    # rospy.wait_for_service("/usb_cam/set_camera_info", 5)
    main()