import cv2 
import sys
import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from nav_msgs.msg import Odometry
import numpy as np


bridge = CvBridge()
class DatasetCreator:
    i = 0
    # def __init__(self):
    # file = open("testfile.txt","w") 
    go = False
    mat = np.zeros((256, 256, 1), dtype = "uint8")
    skip_count = 0
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    def main(self):
      # ic = image_from_ardrone
      topic = "/ardrone/front/image_raw"
      rospy.Subscriber(topic,Image,self.callback)
      print 'Ready to begin.. Run bag file.. topic:', topic
      # rospy.Subscriber("/usb_cam/image_raw",Image,callback)
      rospy.spin()

    def callback(self,data):
        try:
            # rate = rospy.Rate(100)
            # self.go = False
            cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
            (rows,cols,channels) = cv_image.shape
            copy = cv_image.copy()
            cv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
            if self.i == 0:
                ret, corners = cv2.findChessboardCorners(cv_img, (9,6),None)

                if ret == True:
                    print 'Found checkerboard'
                    print self.i
                    cv2.imwrite("/home/saif/msc_workspace/slam_test_bag_dataset2/ardrone_flight_12_08_2017/ardrone_5/data/image_%04d.png"%self.i,copy)
                    self.mat = cv_img
                    self.i += 1
                else:
                    self.skip_count+=1
                    print "skipping", self.skip_count

            else:
                a = cv2.goodFeaturesToTrack(cv_img, 500, 0.01, 45)
                
                if (len(a) > 30 and cv2.countNonZero(cv2.subtract(self.mat,cv_img))>0) and self.i != 0:
                        count = self.i
                        cv2.imwrite("/home/saif/msc_workspace/slam_test_bag_dataset2/ardrone_flight_12_08_2017/ardrone_5/data/image_%04d.png"%count,copy)
                        print count
                        # rospy.Subscriber("/odom",Odometry,self.writeToFile,(copy,count),queue_size=1)
                        self.mat = cv_img
                        self.i +=1
                else:
                        self.skip_count+=1
                        print "skipping", self.skip_count
            print '                                                     --total skipped:', self.skip_count
            # rate.sleep()
        except KeyboardInterrupt:
            cv2.destroyAllWindows()
            rospy.Unsubscribe()

if __name__ == '__main__':
    print "Initialising... Please wait..."
    dc = DatasetCreator()
    rospy.init_node('image_from_ardrone', anonymous=True)
    # rospy.wait_for_service("/ardrone/bottom/set_camera_info", 5)
    # rospy.wait_for_service("/usb_cam/set_camera_info", 5)
    dc.main()
    print "total captured: ", dc.i
    print 'total skipped: ', dc.skip_count
